# Arti

Arti is implementation of Tor's protocols in rust. Rust is a language that provides many benefits over C be it security, flexibility, speed, or anything. You can learn about it more at [Arti](https://tpo.pages.torproject.net/core/arti/#:~:text=Arti%20%E2%80%93%20A%20Tor%20implementation%20in,it%20for%20experimental%20use%20only).

## Overview

This is basic implementation of Arti APIs so as to download a file using various Arti crates. file gets stored in local storage.

## Setup

Install rust and cargo.

Clone this repo, and run `cargo run`
to run the program.

It will download a file named `download.txt` in the current directory.
(one can work around with url to get different files)

## understanding code

1. lets get to understand the main.rs file here and know about how the APIs work.

2.The code first imports the required crates and libraries being used in the code.

3. The code defines constants for the size of each request (REQSIZE), the URL of the file to be downloaded over Tor (TORURL), the URL of a test file (TESTURL), and the name of the download file (DOWNLOAD_FILE_NAME).

4. The code defines an asynchronous function “get_tor_client” that creates a new TorClient instance and returns it. TorClient is a type provided by the arti_client crate, and it allows for isolated connections over the Tor network.

5. The code defines another asynchronous function, “get_new_connection”, that takes a “TorClient” instance as input and creates a new HTTPS connection with a new circuit. The function returns a new “hyper::Client” instance, which is a type provided by the hyper crate that can be used to make HTTP requests.

6. The code defines another asynchronous function, “get_content_length,” that takes a URL and a TorClient instance as inputs and returns the content length of the resource located at the given URL. The function makes an HTTP “GET” request to the URL using a new “hyper::Client” instance created with “get_new_connection”, retrieves the “Content-Length” header from the response, and returns the length as a u64.

7. The code defines another asynchronous function request that takes a URL, a starting byte offset, an ending byte offset, and a “hyper::Client” instance as inputs and returns the response body as a Vec<u8>. The function makes an HTTP “GET” request to the URL with the “Range” header set to the range between the starting and ending byte offsets using the “hyper::Client” instance provided. If the response status code is “206 Partial Content”, the function logs a message and retrieves the response body. If the response status code is anything else, the function logs a message and returns an empty vector.

8. The code defines a synchronous function, “save_to_file,” that takes a file name, a starting byte offset, and a byte vector as inputs and writes the byte vector to the file at the starting byte offset.

9. The function main initializes the logging system, creates a new file with the name DOWNLOAD_FILE_NAME, gets a TorClient instance using “get_tor_client”, gets the content length of the file to be downloaded using get_content_length, sets the length of the download file using “set_len”, calculates the number of steps required to download the file, and loops over each step. For each step, the function creates a new “hyper::Client” instance using “get_new_connection”, sends a request for a specific range of bytes of the file using request, and saves the response body to the download file using “save_to_file”. Finally, the function sends one last request for the remaining bytes and saves the response body to the download file using “save_to_file”.

10. Note that the code makes use of asynchronous programming constructs provided by the tokio crate, such as the async and await keywords, as well as the #[tokio::main] attribute on the main function. This allows the code to perform multiple tasks concurrently and avoid blocking the program's execution while waiting for I/O operations to complete.

so that was how just a basic API can be used. Easy huh 😅.
